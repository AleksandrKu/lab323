<?php
try {
	require_once(__DIR__ . "/File2.php");
	$allFiles = scandir(__DIR__); // Получает список файлов и каталогов, расположенных по указанному пути
	foreach ($allFiles as $file) {
		if ($file == '.' || $file == '..') {
			continue; //если каталог, то пропускаем итерацию
		}
		$fileRout = __DIR__ . '/' . $file; // путь к файлам
		$fileName = new File2($fileRout); // создаем объект класса
		echo "File: " . $fileName->getFileName();
		echo " | size: ";
		echo $fileName->getSize() ? $fileName->getSize() . " |" : " error |";
		echo $fileName->getIsReadable() ? " is readable | " : " is not readable ";
		echo $fileName->getIsWritable() ? " is writable | " : " is not writable ";
		echo " created: " . $fileName->getCreatedDate();
		echo " | modified: " . $fileName->getLastEditedDate() . "<br>";
	}
} catch (Exception $ex) {
	echo 'Error: ' . $ex->getMessage();
}
$file = new File2(__DIR__ . "/test.txt");
echo "<br>" . $file->get_content();

